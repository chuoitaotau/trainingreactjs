import React, { Component } from 'react';
import './App.css';
import ColorPicker from './components/ColorPicker';
import SizeSetting from './components/SizeSetting';
import Reset from './components/Reset';
import Result from './components/Result';

class App extends Component {
  constructor(props){
  	super(props);
  	this.state = {
  		color : 'red',
  		fontSize : 12
  	}
  }
  onSetColor = (params) => {
  	this.setState({
  		color : params
  	});
  }
  onChangeSize = (value) =>{
  	//console.log(value);
	this.setState({
		fontSize : (this.state.fontSize + value >= 8 && this.state.fontSize + value <= 36) ? this.state.fontSize + value : this.state.fontSize
	});
  }
  onSettingDefault = (value) => {
  	this.setState({
  		color : 'red',
  		fontSize : 12
  	});
  }
  render() {
    return (
        <div>
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <a className="navbar-brand">Setting Text</a>
                </div>
            </nav>
            <div className="container">
                <div className="row">
                	<ColorPicker color={this.state.color} onReceiveColor={this.onSetColor}/>
                	<div className="col-xs-6">
                		<SizeSetting 
	                		fontSize={this.state.fontSize} 
	                		onChangeSize={this.onChangeSize}
                		/>
                		<Reset onSettingDefault={this.onSettingDefault}/>
                	</div>
                	<Result fontSize={this.state.fontSize} color={this.state.color}/>
                </div>
            </div>
        </div>
    );
  }
}

export default App;
